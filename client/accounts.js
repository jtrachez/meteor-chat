
Accounts.onLogin(function(){
	Meteor.call('loginUser');
	Session.set('uid', Meteor.userId());
});

Accounts.ui.config({
	passwordSignupFields: "USERNAME_ONLY"
});