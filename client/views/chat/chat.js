Meteor.subscribe('messagesList');
Meteor.subscribe('onlineUsers');



Template.messages.helpers({
  messages: function(){
    var messages = Messages.find({},{sort:{created_at:-1}});
    return messages;
  }
})


Template.onlineUsers.helpers({
  usersOnline: function(){
    return Meteor.users.find({online: 1}).count();
  },
  usersTotal: function(){
    return Meteor.users.find({}).count();
  },
  usersList: function(){
    return Meteor.users.find({online: 1});
  }
})


Template.userInput.events({
  'submit #form': function (event,temp) {
    var input = event.target.input.value;
    Meteor.call('sendMessage',input);
    event.target.input.value = "";
    return false;
  }
});

Template.loginButtons.events({
  'click #login-buttons-logout': function (event) {
    Meteor.call('logoutUser',Session.get('uid'));
    delete Session.keys['uid'];
  }
});


UI.registerHelper('formatTime', function(context, options) {
  if(context)
    return moment(context).format('DD/MM/YYYY hh:mm');
});



