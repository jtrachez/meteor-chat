Meteor.methods({

  loginUser : function(){
    Meteor.users.update(Meteor.userId(), {$set:{'status': 1,'online': 1}});
    console.log('[ User connected ]');
  },

  sendMessage : function(message){
    Messages.insert({
      message: message,
      user_id: Meteor.userId(),
      username: Meteor.user().username,
      created_at: Date.now()
    })
    console.log('[ New message ]');
  },

  logoutUser : function(uid){
    Meteor.users.update(
      uid,
      {$set:{'online': 0}}
      );
    console.log('[ User disconnected ] ' + Meteor.userId());
  }

})